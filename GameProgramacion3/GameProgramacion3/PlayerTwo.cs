﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consoleGame
{
    class PlayerTwo
    {
        private int Player2LocationX;
        private int Player2LocationY;
        private bool Player2UP, Player2DOWN, Player2LEFT, Player2RIGHT;
        private PlayerOne c1 = new PlayerOne();

        public PlayerTwo()
        {

            Player2LocationX = 1;
            Player2LocationY = 0;
            Player2UP = Player2DOWN = Player2LEFT = Player2RIGHT = true;
        }

        public void PlayerTwoMovement(ConsoleKeyInfo key_pushed)
        {

            switch (key_pushed.Key)
            {
                case ConsoleKey.A:
                    if (Player2LocationX > 1 && Player2LEFT)
                    {
                        Player2LocationX -= 2;
                        Player2LEFT = false;
                        Player2RIGHT = Player2DOWN = Player2UP = true;
                    }
                    break;

                case ConsoleKey.D:
                    if (Player2LocationX < 77 && Player2RIGHT)
                    {
                        Player2LocationX += 2;
                        Player2RIGHT = false;
                        Player2LEFT = Player2UP = Player2DOWN = true;
                    }
                    break;

                case ConsoleKey.W:
                    if (Player2LocationY > 1 && Player2UP)
                    {
                        Player2LocationY -= 2;
                        Player2UP = false;
                        Player2DOWN = Player2LEFT = Player2RIGHT = true;
                    }
                    break;

                case ConsoleKey.S:
                    if (Player2LocationY < 23 && Player2DOWN)
                    {
                        Player2LocationY += 2;
                        Player2DOWN = false;
                        Player2UP = Player2RIGHT = Player2LEFT = true;
                    }
                    break;

                case ConsoleKey.Escape:
                    Game.gameLoopfortwo = false;
                    break;
            }

        }

        public void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.SetCursorPosition(Player2LocationX, Player2LocationY);
            Console.Write("-");
        }

        public void EnemyCollision(Enemy e)
        {
            if (Player2LocationX == e.GetLocationX()
                && Player2LocationY == e.GetLocationY())
            {
                Game.gameLoopfortwo = false;
            }
        }

        public void StaticEnemyCollision(StaticEnemy SE)
        {
            //AGREGAR COSAS
        }

        public void CheckpointCollision(Checkpoint cp)
        {
            if ((Player2LocationX == cp.GetLocationX()
                && Player2LocationY == cp.GetLocationY())
                || (Player2LocationX == cp.GetLocationX() + 1
                && Player2LocationY == cp.GetLocationY() + 1))
            {
                Game.gameLoopfortwo = false;
                Game.win = true;
            }
        }

        public int GetLocationX()
        {
            return Player2LocationX;
        }

        public int GetLocationY()
        {
            return Player2LocationY;
        }

        public void SetLocationX(int x)
        {
            Player2LocationX = x;
        }

        public void SetLocationY(int y)
        {
            Player2LocationY = y;
        }
    }
}


