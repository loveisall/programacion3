﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consoleGame
{
    class Level
    {
        private int cuantEnemy = 30;
        private int cuantScore = 15;
        private int cuantStaticEnemy = 10;
        private PlayerOne p1;
        private Enemy[] es;
        private Checkpoint cp;
        private ScorePoints[] SP;
        private PlayerTwo p2;
        private StaticEnemy[] SE;
        ConsoleKeyInfo userkey;

        public Level()
        {
            p1 = new PlayerOne();
            es = new Enemy[cuantEnemy];
            cp = new Checkpoint();
            p2 = new PlayerTwo();
            SP = new ScorePoints[cuantScore];
            SE = new StaticEnemy[cuantStaticEnemy];

            Random r = new Random();
            for (int i = 0; i < cuantEnemy; i++)
                es[i] = new Enemy(r.Next(0, 78), r.Next(2, 24));
            for (int i = 0; i < cuantScore; i++)
                SP[i] = new ScorePoints(r.Next(0, 78), r.Next(2, 24));
            for (int i = 0; i < cuantStaticEnemy; i++)
                SE[i] = new StaticEnemy(r.Next(0, 78), r.Next(2, 24));

        }

        public void RunForOnePlayer()
        {
            if (Console.KeyAvailable)
            {
                userkey = Console.ReadKey(true);
                p1.Movement(userkey);
            }
            for (int i = 0; i < cuantEnemy; i++)
            {
                es[i].Movement();
                p1.EnemyCollision(es[i]);
            }
            for (int i = 0; i < cuantScore; i++)
            {
                p1.ScorePointsCollision(SP[i]);
            }

            for (int i = 0; i < cuantStaticEnemy; i++) {
                p1.StaticEnemyCollision(SE[i]);              
            }
            p1.CheckpointCollision(cp);
            Console.Clear();
            DrawForOnePlayer();
            p1.DrawScore();
            p1.DrawPlayerLifes();
        }
        public void RunForTwoPlayers()
        {
            if (Console.KeyAvailable)
            {
                userkey = Console.ReadKey(true);
                p1.Movement(userkey);
                p2.PlayerTwoMovement(userkey);
            }
            for (int i = 0; i < cuantEnemy; i++)
            {
                es[i].Movement();
                p1.EnemyCollisionWhilePlayingTwO(es[i]);
                p2.EnemyCollision(es[i]);
            }

            p1.CheckPointcollisionWhilePlayingTWO(cp);
            p2.CheckpointCollision(cp);
            Console.Clear();
            DrawForTwoPlayers();
        }

        public void DrawForOnePlayer()
        {
            p1.Draw();
            cp.Draw();
            for (int i = 0; i < cuantEnemy; i++)
                es[i].Draw();
            for (int i = 0; i < cuantScore; i++)
                SP[i].Draw();
            for (int i = 0; i < cuantStaticEnemy; i++)
                SE[i].Draw();
        }

        public void DrawForTwoPlayers()
        {
            p1.Draw();
            p2.Draw();
            cp.Draw();
            for (int i = 0; i < cuantEnemy; i++)
            {
                es[i].Draw();
            }
        }
    }
}
