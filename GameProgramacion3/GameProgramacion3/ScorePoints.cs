﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consoleGame
{
    class ScorePoints
    {
        public int ScoreLocationX;
        public int ScoreLocationY;
        public string ScorePAINT = "o";

        public ScorePoints(int x, int y)
        {
            ScoreLocationX = x;
            ScoreLocationY = y;
        }

        public void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.SetCursorPosition(ScoreLocationX, ScoreLocationY);
            Console.Write(ScorePAINT);
        }

        public int GetScoreLocationX()
        {
            return ScoreLocationX;
        }

        public int GetScoreLocationY()
        {
            return ScoreLocationY;
        }

        public void SetScoreLocationX(int x)
        {
            ScoreLocationX = x;
        }

        public void SetScoreLocationY(int y)
        {
            ScoreLocationY = y;
        }
    }
}
