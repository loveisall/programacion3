﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consoleGame
{
    class StaticEnemy
    {
        public int StaticLocationX;
        public int StaticLocationY;
        public string StaticEnemyPaint = "X";

        public StaticEnemy(int x, int y)
        {
            StaticLocationX = x;
            StaticLocationY = y;
        }

        public void Draw()
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.SetCursorPosition(StaticLocationX, StaticLocationY);
            Console.Write(StaticEnemyPaint);
        }

        public int GetStaticEnemyLocationX()
        {
            return StaticLocationX;
        }

        public int GetStaticEnemyLocationY()
        {
            return StaticLocationY;
        }

        public void SetStaticEnemyLocationX(int x)
        {
            StaticLocationX = x;
        }

        public void SetStaticEnemyLocationY(int y)
        {
            StaticLocationY = y;
        }
    }
}
