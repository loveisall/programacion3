﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace consoleGame
{
    class Game
    {
        public static bool gameLoop;
        public static bool gameLoopfortwo;
        public static bool win;
        private Level actual;
        private ConsoleKeyInfo userKey;

        public bool ShowWelcomeText;
        public string textoaguardar;
        public string textoarchivo;


        PlayerOne p1 = new PlayerOne();

        public void MensajeBienvenida()
        {
            if (File.Exists("datosJuego.txt"))
            {
                FileStream fs = File.OpenRead("datosJuego.txt");
                StreamReader sr = new StreamReader(fs);
                textoarchivo = sr.ReadLine();
                ShowWelcomeText = true;
                sr.Close();
                fs.Close();
            }
            else
            {
                FileStream fs = File.Create("datosJuego.txt");
                StreamWriter sw = new StreamWriter(fs);
                Console.WriteLine("TYPE YOUR WELCOME TEXT");
                textoaguardar = Console.ReadLine();
                ShowWelcomeText = false;
                sw.Close();
                fs.Close();
                Console.Clear();
            }
        }


        public Game()
        {
            Console.CursorVisible = false;
            win = false;
            actual = new Level();
        }


        public void Run()
        {
            Console.ForegroundColor = ConsoleColor.White;
            if (ShowWelcomeText)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.SetCursorPosition(Console.WindowWidth / 2 - 4, Console.WindowHeight / 2 - 4);
                Console.WriteLine(textoarchivo);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.SetCursorPosition(Console.WindowWidth / 2 - 4, Console.WindowHeight / 2 - 2);
                Console.WriteLine(textoaguardar);
            }
            Console.SetCursorPosition(Console.WindowWidth / 2 - 15, Console.WindowHeight / 2 + 1);
            Console.WriteLine("PRESS F1 TO BEGIN WITH ONE PLAYER");
            Console.SetCursorPosition(Console.WindowWidth / 2 - 15, Console.WindowHeight / 2 + 2);
            Console.WriteLine("PRESS F2 TO BEGIN WITH TWO PLAYERS");
            Console.SetCursorPosition(Console.WindowWidth / 2 - 15, Console.WindowHeight / 2 + 3);
            Console.Write("PRESS ESCAPE TWICE TO EXIT");

            userKey = Console.ReadKey();
            if (userKey.Key == ConsoleKey.Escape)
                return;

            if (userKey.Key == ConsoleKey.F1)
            {
                gameLoop = true;

            }
            if (userKey.Key == ConsoleKey.F2)
            {
                gameLoopfortwo = true;
            }



            while (gameLoop)
            {
                actual.RunForOnePlayer();
                System.Threading.Thread.Sleep(50);
            }

            while (gameLoopfortwo)
            {
                actual.RunForTwoPlayers();
                System.Threading.Thread.Sleep(50);
            }

            Console.Clear();
            if (win)
            {
                Console.ForegroundColor = ConsoleColor.White;
                IngreseNombreParaRecibirScore();
                Console.SetCursorPosition(Console.WindowWidth / 2 - 7, Console.WindowHeight / 2);
                Console.WriteLine("YOU WIN!");
                Console.SetCursorPosition(Console.WindowWidth / 2 - 12, Console.WindowHeight / 2 + 1);
                Console.WriteLine("PRESS ESCAPE TO QUIT");
                Console.SetCursorPosition(Console.WindowWidth / 2 - 13, Console.WindowHeight / 2 + 2);
                Console.WriteLine("PRESS F1 TO RESTART");
                Console.ReadKey();
            }
            else
            {

                Console.ForegroundColor = ConsoleColor.Red;
                IngreseNombreParaRecibirScore();
                Console.SetCursorPosition(Console.WindowWidth / 2 - 8, Console.WindowHeight / 2);
                Console.WriteLine("YOU LOOSE & YOU SUCK!");
                Console.SetCursorPosition(Console.WindowWidth / 2 - 8, Console.WindowHeight / 2 + 1);
                Console.WriteLine("PRESS ESCAPE TO QUIT");
                Console.SetCursorPosition(Console.WindowWidth / 2 - 8, Console.WindowHeight / 2 + 2);
                Console.WriteLine("PRESS F1 TO RESTART");
                Console.ReadKey();
            }
        }

        void IngreseNombreParaRecibirScore()
        {
            if (File.Exists("datosScore.txt"))
            {
                FileStream fs = File.OpenRead("datosScore.txt");
                BinaryReader br = new BinaryReader(fs);
                int maxscore = br.ReadInt32();
                Console.SetCursorPosition(Console.WindowWidth / 2 - 8, Console.WindowHeight / 2 - 2);
                Console.WriteLine("THE HIGHEST SCORE IS:" + maxscore);
                br.Close();
                fs.Close();
            }
            else
            {
                FileStream fs = File.Create("datosScore.txt");
                BinaryWriter bw = new BinaryWriter(fs);
                Console.SetCursorPosition(Console.WindowWidth / 2 - 8, Console.WindowHeight / 2 - 2);
                Console.WriteLine("TYPE YOUR NAME: ");
                string nombre = Console.ReadLine();
                Console.Clear();
                Console.SetCursorPosition(Console.WindowWidth / 2 - 8, Console.WindowHeight / 2 - 2);
                Console.WriteLine(nombre + " YOUR SCORE IS: " + p1.SCORE);
                bw.Write(p1.SCORE);
                bw.Close();
                fs.Close();
            }
        }
    }
}
