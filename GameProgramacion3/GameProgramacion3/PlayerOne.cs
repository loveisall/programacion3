﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consoleGame
{
    class PlayerOne
    {
        public int locationX;
        public int locationY;
        private bool UP, DOWN, LEFT, RIGHT;
        public int SCORE = 1;
        public int Lifes = 3;
        public PlayerOne()
        {
            locationX = 0;
            locationY = 0;
            UP = DOWN = LEFT = RIGHT = true;
        }

        public void Movement(ConsoleKeyInfo key_pushed)
        {
            switch (key_pushed.Key)
            {
                case ConsoleKey.LeftArrow:
                    if (locationX > 1 && LEFT)
                    {
                        locationX -= 1;
                        LEFT = false;
                        RIGHT = DOWN = UP = true;
                    }
                    break;
                case ConsoleKey.RightArrow:
                    if (locationX < 77 && RIGHT)
                    {
                        locationX += 1;
                        RIGHT = false;
                        LEFT = UP = DOWN = true;
                    }
                    break;
                case ConsoleKey.UpArrow:
                    if (locationY > 1 && UP)
                    {
                        locationY -= 1;
                        UP = false;
                        DOWN = LEFT = RIGHT = true;
                    }
                    break;
                case ConsoleKey.DownArrow:
                    if (locationY < 23 && DOWN)
                    {
                        locationY += 1;
                        DOWN = false;
                        UP = RIGHT = LEFT = true;
                    }
                    break;

                case ConsoleKey.Escape:
                    Game.gameLoop = false;
                    break;
            }
        }

        public void Draw()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(locationX, locationY);
            Console.Write("+");
        }

        public void DrawPlayerLifes()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(Console.WindowWidth - 15, Console.WindowHeight - 24);
            Console.Write("♥");
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(Console.WindowWidth - 14, Console.WindowHeight - 24);
            Console.Write(" x" + Lifes);

        }

        public void DrawScore()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.SetCursorPosition(Console.WindowWidth - 10, Console.WindowHeight - 24);
            Console.Write("Score");
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(Console.WindowWidth - 4, Console.WindowHeight - 24);
            Console.Write("x" + SCORE);
        }

        public void EnemyCollision(Enemy e)
        {
            if (locationX == e.GetLocationX()
                && locationY == e.GetLocationY())
            {
                Lifes--;
                if (Lifes <= 0)
                {
                    Game.gameLoop = false;
                }
            }
        }

        public void StaticEnemyCollision(StaticEnemy SE)
        {
            if (locationX == SE.GetStaticEnemyLocationX()
                && locationY == SE.GetStaticEnemyLocationY())
            {
                Lifes--;
                SE.StaticEnemyPaint = null;
                SE.StaticLocationX = 0;
                SE.StaticLocationY = 0;
                if (Lifes <= 0)
                {
                    Game.gameLoop = false;
                }
            }
        }
        public void ScorePointsCollision(ScorePoints SP)
        {
            if (locationX == SP.GetScoreLocationX()
                && locationY == SP.GetScoreLocationY())
            {
                SCORE++;
                SP.ScorePAINT = null;
                SP.ScoreLocationX = 0;
                SP.ScoreLocationY = 0;
            }
        }

        public void EnemyCollisionWhilePlayingTwO(Enemy e)
        {
            if (locationX == e.GetLocationX()
                && locationY == e.GetLocationY())
            {
                Game.gameLoopfortwo = false;
            }
        }

        public void CheckpointCollision(Checkpoint cp)
        {
            if ((locationX == cp.GetLocationX()
                && locationY == cp.GetLocationY())
                || (locationX == cp.GetLocationX() + 1
                && locationY == cp.GetLocationY() + 1))
            {
                Game.gameLoop = false;
                Game.win = true;
            }
        }
        public void CheckPointcollisionWhilePlayingTWO(Checkpoint cp)
        {
            if ((locationX == cp.GetLocationX()
                            && locationY == cp.GetLocationY())
                            || (locationX == cp.GetLocationX() + 1
                            && locationY == cp.GetLocationY() + 1))
            {
                Game.gameLoopfortwo = false;
                Game.win = true;
            }
        }
        public int GetLocationX()
        {
            return locationX;
        }

        public int GetLocationY()
        {
            return locationY;
        }

        public void SetLocationX(int x)
        {
            locationX = x;
        }

        public void SetLocationY(int y)
        {
            locationY = y;
        }
    }
}
