﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Net;
namespace consoleGame
{
    class Program
    {
        static void Main(string[] args)
        {
            //-----------------------------------------------------------------------------------------------------------------------------
            //Esto es para cambiar la pantalla del juego de acuerdo al clima
            string PlaceToObtainWeather = "BuenosAires";
            //https://query.yahooapis.com/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22BuenosAires%2C%20ar%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys
            WebRequest request = WebRequest.Create("https://query.yahooapis.com/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + PlaceToObtainWeather + "%2C%20ar%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
            //WebRequest request = WebRequest.Create("https://query.yahooapis.com/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22dallas%2C%20tx%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");

            WebResponse response = request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader sr = new StreamReader(stream);
            JObject data = JObject.Parse(sr.ReadToEnd());
            string read = sr.ReadToEnd();
            string tiempo = (string)data["query"]["results"]["channel"]["item"]["condition"]["text"];
            switch (tiempo)
            {
                case "Cloudy":
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    break;
                case "Breezy":
                    Console.BackgroundColor = ConsoleColor.DarkCyan;
                    break;
                case "Sunny":
                    Console.BackgroundColor = ConsoleColor.DarkYellow;
                    break;
                case "Clear":
                    Console.BackgroundColor = ConsoleColor.Black;
                    break;
                default:
                    Console.BackgroundColor = ConsoleColor.Black;
                    break;
            }
            //-----------------------------------------------------------------------------------------------------------------------------
            ConsoleKeyInfo userKey;
            Console.Title = "CONSOLE GAME";
            Game g = new Game();



            g.MensajeBienvenida();
            g.Run();
            do
            {

                userKey = Console.ReadKey();
                if (userKey.Key != ConsoleKey.Escape)
                    break;
            } while (userKey.Key == ConsoleKey.F1);
            Console.Clear();
        }
    }
}
